package bg.swift.socialsystem;

import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.education.EducationComparator;
import bg.swift.socialsystem.education.EducationRepository;
import bg.swift.socialsystem.insurance.SocialInsuranceComparator;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import bg.swift.socialsystem.insurance.SocialInsuranceRecordRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Controller
public class PeopleController {

    private final PersonRepository personRepository;
    private final EducationRepository educationRepository;
    private final SocialInsuranceRecordRepository socialInsuranceRecordRepository;

    private List<Person> people;

    public PeopleController(PersonRepository personRepository, EducationRepository educationRepository,
                            SocialInsuranceRecordRepository socialInsuranceRecordRepository) {
        this.personRepository = personRepository;
        this.educationRepository = educationRepository;
        this.socialInsuranceRecordRepository = socialInsuranceRecordRepository;
    }

    @GetMapping("/people")
    public String getPeople(Model model,
                            @RequestParam(value = "firstName", required = false)
                                    String firstName,
                            @RequestParam(value = "lastName", required = false)
                                    String lastName) {

        Iterable<Person> allPeople = personRepository.findAll();
        people = (List<Person>) allPeople;

        List<Person> filteredPeople;

        if (firstName == null && lastName == null) {
            filteredPeople = people;
        } else {
            filteredPeople = new ArrayList<>();
            if (!(firstName == null) && "".equals(lastName)) {
                for (Person p : people) {
                    if (firstName.equals(p.getFirstName())) {
                        filteredPeople.add(p);
                    }
                }
            }
            if ("".equals(firstName)) {
                for (Person p : people) {
                    if (lastName.equals(p.getLastName())) {
                        filteredPeople.add(p);
                    }
                }
            }
            if (!(firstName == null) && !(lastName == null)) {
                for (Person p : people) {
                    if (firstName.equals(p.getFirstName()) && lastName.equals(p.getLastName())) {
                        filteredPeople.add(p);
                    }
                }
            }
        }

        model.addAttribute("people", filteredPeople);

        return "people";
    }

    @GetMapping("/people/{id}")
    public String getPersonBy(@PathVariable("id") Integer id,
                              Model model) {

        Person person = personRepository.findById(id).get();

        List<Education> sortedEducationRecords = new ArrayList<>(person.getEducationRecords());
        Collections.sort(sortedEducationRecords, new EducationComparator<Education>());

        List<SocialInsuranceRecord> sortedSocialInsuranceRecords = new ArrayList<>(person.getSocialInsuranceRecords());
        Collections.sort(sortedSocialInsuranceRecords, new SocialInsuranceComparator<SocialInsuranceRecord>());

//        List<SocialInsuranceRecord> sortedSocialInsuranceRecords = socialInsuranceRecordRepository.findAllByOrderByYearDescMonthDesc();
//        List<Education> sortedEducationRecords = educationRepository.findAllByOrderByEnrollmentDateDesc();
        model.addAttribute("sortedSocialInsuranceRecords", sortedSocialInsuranceRecords);
        model.addAttribute("sortedEducationRecords", sortedEducationRecords);

        model.addAttribute("person", person);

        return "person-details";
    }

    @PostMapping("/people/{id}")
    public String addNewSocialInsurnceRecord(
            @PathVariable("id") Integer id,
            Model model,
            @ModelAttribute
            SocialInsuranceRecord newSocialInsuranceRecord
    ) {
        Iterable<Person> allPeople = personRepository.findAll();
        people = (List<Person>) allPeople;

        Person person = people.get(id - 1);
        person.addSocialInsuranceRecord(newSocialInsuranceRecord);

        personRepository.save(person);

        model.addAttribute("person", person);
        model.addAttribute("newSocialInsuranceRecord", newSocialInsuranceRecord);

        return getPersonBy(id, model);
    }
}
