package bg.swift.socialsystem.insurance;

import javax.persistence.*;

@Entity
public class SocialInsuranceRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private int year;
    private int month;

    private double amount;

    private SocialInsuranceRecord(){

    }

    public SocialInsuranceRecord(double amount, int year, int month){
        this.amount = amount;
        this.year = year;
        this.month = month;
    }

    public double getAmount(){
        return amount;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }
}
