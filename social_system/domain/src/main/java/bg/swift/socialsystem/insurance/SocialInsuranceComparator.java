package bg.swift.socialsystem.insurance;

import java.util.Comparator;

public class SocialInsuranceComparator<T> implements Comparator<SocialInsuranceRecord> {
    @Override
    public int compare(SocialInsuranceRecord socialInsuranceRecord1, SocialInsuranceRecord socialInsuranceRecord2) {
        if (socialInsuranceRecord1.getYear() > socialInsuranceRecord2.getYear()) {
            return -1;
        } else if (socialInsuranceRecord1.getYear() == socialInsuranceRecord2.getYear()) {
            return socialInsuranceRecord1.getMonth() > socialInsuranceRecord2.getMonth() ? -1 : socialInsuranceRecord1.getMonth() == socialInsuranceRecord2.getMonth() ? 0 : 1;
        }
        return 1;

//        return socialInsuranceRecord1.getYear() > socialInsuranceRecord2.getYear() ? -1 : socialInsuranceRecord1.getYear() == socialInsuranceRecord2.getYear() ? 0 : 1;
    }
}
