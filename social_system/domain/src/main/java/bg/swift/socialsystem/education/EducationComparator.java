package bg.swift.socialsystem.education;

import java.util.Comparator;

public class EducationComparator<E> implements Comparator<Education> {

    @Override
    public int compare(Education education1, Education education2) {
        return education1.getEnrollmentDate().isAfter(education2.getEnrollmentDate()) ? -1 : education1.getEnrollmentDate().isEqual(education2.getEnrollmentDate()) ? 0 : 1;
    }
}
