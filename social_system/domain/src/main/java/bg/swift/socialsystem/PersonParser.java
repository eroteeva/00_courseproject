package bg.swift.socialsystem;

import bg.swift.socialsystem.address.Address;
import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.education.EducationDegree;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import bg.swift.socialsystem.insurance.YearMonth;
import bg.swift.socialsystem.utils.PersonUtils;

import java.time.LocalDate;
import java.util.*;

public class PersonParser {

    public static Person createPersonFromInput(String personalDataInput, String educationInfoInput, String socialInsuranceInfoInput){

        String[] personalData = personalDataInput.split(";");

        String firstName = personalData[0].trim();
        String lastName = personalData[1].trim();
        Gender gender = validateGender(personalData[2]);
        int height = Integer.valueOf(personalData[3]);
        LocalDate birthday = PersonUtils.parseDateFromInput(personalData[4]);

        Address address = createAddressFromInput(Arrays.asList(personalData).subList(5, personalData.length));

        Set<Education> educationRecords = createEducationRecordsFromInput(educationInfoInput);

        Set<SocialInsuranceRecord> socialInsuranceRecords = createSocialInsuranceRecordsFromInput(socialInsuranceInfoInput);

        return new Person(firstName, lastName, gender, height, birthday, address, educationRecords, socialInsuranceRecords);

    }

    private static Gender validateGender(String gender){
        if ("M".equals(gender)){
            return Gender.MALE;
        }
        return Gender.FEMALE;
    }

    private static Address createAddressFromInput(List<String> addressElements){

        String country = addressElements.get(0);
        String city = addressElements.get(1);
        String municipality = addressElements.get(2);
        String postcode = addressElements.get(3);
        String streetName = addressElements.get(4);
        int streetNumber = Integer.valueOf(addressElements.get(5));

        return new Address(country, city, municipality, postcode, streetName, streetNumber);
    }

    private static Set<Education> createEducationRecordsFromInput(String educationInfoInput) {

        Set<Education> educationRecords = new HashSet<>();

        if ("".equals(educationInfoInput)){
            System.out.println("No data was available for education records.");
            return educationRecords;
        }

        String[] educationInfo = educationInfoInput.split(";", -1);
        int numberOfRecords = educationInfo.length / 6;


        for (int i = 0; i < numberOfRecords; i++){

            EducationDegree educationDegree;
            switch(educationInfo[0 + i*6].toUpperCase()){
                case "P":
                    educationDegree = EducationDegree.PRIMARY;
                    break;
                case "S":
                    educationDegree = EducationDegree.SECONDARY;
                    break;
                case "B":
                    educationDegree = EducationDegree.BACHELOR;
                    break;
                case "M":
                    educationDegree = EducationDegree.MASTER;
                    break;
                case "D":
                    educationDegree = EducationDegree.DOCTORATE;
                    break;
                default:
                    System.out.println("No information about education type was provided!");
                    educationDegree = EducationDegree.NONE;
                    break;
            }

            String institutionName = educationInfo[1 + i*6].trim();
            LocalDate enrollmentDate = PersonUtils.parseDateFromInput(educationInfo[2 + i*6].trim());
            LocalDate graduationDate = PersonUtils.parseDateFromInput(educationInfo[3 + i*6].trim());
            boolean hasGraduated = false;
            if ("YES".equals(educationInfo[4 + i*6].trim())){
                hasGraduated = true;
            }

            Education education = new Education(educationDegree, institutionName, enrollmentDate, graduationDate, hasGraduated);
            if (!"".equals(educationInfo[5 + i*6])){
                education.setFinalGrade(Double.valueOf(educationInfo[5 + i*6]));
            }

            educationRecords.add(education);
        }
        return educationRecords;
    }

    private static Set<SocialInsuranceRecord> createSocialInsuranceRecordsFromInput(String socialInsuranceInfoInput) {

        Set<SocialInsuranceRecord> socialInsuranceRecords = new HashSet<>();

        if ("".equals(socialInsuranceInfoInput)){
            System.out.println("No data was available for social insurance records.");
            return socialInsuranceRecords;
        }

        String[] insuranceRecords = socialInsuranceInfoInput.split(";");
        double amount;
        int year;
        int month;



        for (int i = 0; i < insuranceRecords.length; i += 3){
            amount = Double.valueOf(insuranceRecords[i]);
            year = Integer.valueOf(insuranceRecords[i + 1]);
            month = Integer.valueOf(insuranceRecords[i + 2]);

            SocialInsuranceRecord record = new SocialInsuranceRecord(amount, year, month);
            socialInsuranceRecords.add(record);
        }
        return socialInsuranceRecords;
    }
}
