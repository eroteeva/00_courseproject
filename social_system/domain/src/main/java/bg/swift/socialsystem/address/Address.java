package bg.swift.socialsystem.address;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String country;
    private String city;
    private String municipality;
    private String postcode;
    private String streetName;
    private int streetNumber;

    private Address(){

    }

    public Address(String country, String city, String municipality, String postcode, String streetName, int streetNumber){
        this.country = country;
        this.city = city;
        this.municipality = municipality;
        this.postcode = postcode;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
    }

    public String getCountry(){
        return country;
    }

    public String getMunicipality(){
        return municipality;
    }

    public String getStreetName(){
        return streetName;
    }

    public int getStreetNumber(){
        return streetNumber;
    }

    public String getPostcode(){
        return postcode;
    }

    public String getCity(){
        return city;
    }

}
