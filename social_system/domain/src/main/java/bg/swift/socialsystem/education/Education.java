package bg.swift.socialsystem.education;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Education {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private EducationDegree educationDegree;

    private String institutionName;
    private LocalDate enrollmentDate;
    private LocalDate graduationDate;
    private boolean hasGraduated;

    private double finalGrade;

    private Education(){

    }


    public Education(EducationDegree educationDegree, String institutionName, LocalDate enrollmentDate, LocalDate graduationDate, boolean hasGraduated){
        this.educationDegree = educationDegree;
        this.institutionName = institutionName;
        this.enrollmentDate = enrollmentDate;
        this.graduationDate = graduationDate;
        this.hasGraduated = hasGraduated;
    }

    public EducationDegree getEducationDegree(){
        return this.educationDegree;
    }

    public String getInstitutionName(){
        return institutionName;
    }

    public LocalDate getEnrollmentDate(){
        return enrollmentDate;
    }

    public LocalDate getGraduationDate(){
        return graduationDate;
    }

    public boolean hasGraduated(){
        return hasGraduated;
    }

    public double getFinalGrade(){
        return this.finalGrade;
    }

    public void setFinalGrade(double finalGrade){
        this.finalGrade = finalGrade;
    }

    public boolean isGraduationDateInTheFuture(){
        if (this.graduationDate.isAfter(LocalDate.now())){
            return true;
        }
        return false;
    }
}
