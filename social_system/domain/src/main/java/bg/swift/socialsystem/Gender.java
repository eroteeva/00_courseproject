package bg.swift.socialsystem;

public enum Gender {

    MALE("M"),
    FEMALE("F");

    private String abbreviation;

    Gender(String genderAbbreviation){
        this.abbreviation = genderAbbreviation;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

}
