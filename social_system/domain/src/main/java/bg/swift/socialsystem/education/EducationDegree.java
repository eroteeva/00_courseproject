package bg.swift.socialsystem.education;

public enum EducationDegree {

    NONE(""),
    PRIMARY("P"),
    SECONDARY("S"),
    BACHELOR("B"),
    MASTER("M"),
    DOCTORATE("D");

    private String abbreviation;

    EducationDegree(String abbreviation){
        this.abbreviation = abbreviation;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}
