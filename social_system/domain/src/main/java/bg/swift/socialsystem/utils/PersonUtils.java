package bg.swift.socialsystem.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PersonUtils {

    public static LocalDate parseDateFromInput(String inputDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy");
        LocalDate date = LocalDate.parse(inputDate, formatter);
        return date;
    }
}
