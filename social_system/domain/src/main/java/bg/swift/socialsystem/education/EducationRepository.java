package bg.swift.socialsystem.education;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EducationRepository extends CrudRepository<Education, Integer> {

    List<Education> findAllByOrderByEnrollmentDateDesc();
}
