package bg.swift.socialsystem.insurance;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SocialInsuranceRecordRepository extends CrudRepository<SocialInsuranceRecord, Integer> {

    List<SocialInsuranceRecord> findAllByOrderByYearDescMonthDesc();
}
