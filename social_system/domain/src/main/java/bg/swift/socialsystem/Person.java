package bg.swift.socialsystem;

import bg.swift.socialsystem.address.Address;
import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

import static java.time.temporal.ChronoUnit.MONTHS;

@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;
    private String lastName;

    @Enumerated(EnumType.STRING)
    private Gender gender;
    private int height;
    private LocalDate birthday;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Education> educationRecords;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<SocialInsuranceRecord> socialInsuranceRecords;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Address address;

    private Person(){

    }

    Person(String firstName, String lastName, Gender gender, int height, LocalDate birthday, Address address,
                   Set<Education> educationRecords,
                   Set<SocialInsuranceRecord> socialInsuranceRecords){
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.height = height;
        this.birthday = birthday;
        this.address = address;
        this.educationRecords = educationRecords;
        this.socialInsuranceRecords = socialInsuranceRecords;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getBirthday(){
        return birthday;
    }

    public int getHeight(){
        return height;
    }

    public Gender getGender(){
        return gender;
    }

    public Set<Education> getEducationRecords() {
        return educationRecords;
    }

    public Set<SocialInsuranceRecord> getSocialInsuranceRecords() {
        return socialInsuranceRecords;
    }

    public void addSocialInsuranceRecord(SocialInsuranceRecord record){
        socialInsuranceRecords.add(record);
    }

    public String getFormattedAddress(){
        return String.format(this.address.getStreetNumber() + " " + this.address.getStreetName() + " Street%n"
        + this.address.getPostcode() + " " + this.address.getMunicipality() + "%n"
        + this.address.getCity() + ", " + this.address.getCountry());
    }

    public boolean isEligibleForSocialAid(){
        boolean hasGraduatedSecondaryEducation = false;
        for (Education education : getEducationRecords()){
            if (education.hasGraduated()){
                hasGraduatedSecondaryEducation = true;
                break;
            }
        }

        boolean hasInsuranceInTheLastThreeMonths = false;
        for (SocialInsuranceRecord socialInsuranceRecord : getSocialInsuranceRecords()){
            LocalDate insuranceDate = LocalDate.of(socialInsuranceRecord.getYear(), socialInsuranceRecord.getMonth(), 1);
            if (insuranceDate.isAfter(LocalDate.now().minus(3, MONTHS))){
                hasInsuranceInTheLastThreeMonths = true;
                break;
            }
        }

        return hasGraduatedSecondaryEducation && !hasInsuranceInTheLastThreeMonths;
    }

    public double getSocialAidAmount(){
        double socialAidAmount = 0;
        for (SocialInsuranceRecord socialInsuranceRecord : getSocialInsuranceRecords()){
            LocalDate insuranceDate = LocalDate.of(socialInsuranceRecord.getYear(), socialInsuranceRecord.getMonth(), 1);
            LocalDate insuranceStartDate = LocalDate.now().minus(27, MONTHS);
            if (insuranceDate.isAfter(LocalDate.now().minus(27, MONTHS))){
                socialAidAmount += socialInsuranceRecord.getAmount();
            }
        }
        return socialAidAmount/24;
    }

    public String getSocialAidText(){
        if (this.isEligibleForSocialAid()){
            return String.format("%.2f", this.getSocialAidAmount());
        }
        return "NOT ELIGIBLE";
    }
}
