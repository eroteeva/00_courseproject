package bg.swift.socialsystem;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;
import static bg.swift.socialsystem.PersonParser.*;

import java.util.Optional;
import java.util.Scanner;

@Service
public class PeopleRegistrationService implements ApplicationRunner {

    private final PersonRepository repository;

    public PeopleRegistrationService(PersonRepository repository) {
        this.repository = repository;
    }

    @java.lang.Override
    public void run(ApplicationArguments args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int numberOfPeople;
        System.out.println("Enter number of people:");
        numberOfPeople = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < numberOfPeople; i++) {
            String personalData = scanner.nextLine();
            String educationInfo = scanner.nextLine();
            String socialInsuranceInfo = scanner.nextLine();
            Person person = createPersonFromInput(personalData, educationInfo, socialInsuranceInfo);
            repository.save(person);
        }
    }
}
